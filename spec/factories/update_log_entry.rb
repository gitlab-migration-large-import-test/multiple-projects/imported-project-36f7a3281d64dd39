# frozen_string_literal: true

FactoryBot.define do
  factory :update_log_entry, class: "Update::LogEntry" do
    timestamp { Time.zone.now }
    message { "test log message" }
    level { 1 }

    run { create(:update_run) }
  end
end
